// Shows simple verification modal to confirm delete action
function verifyDelete(value) {
  var modal = document.getElementById('deleteModal');
  var confirmYes = document.getElementById('confirmYes');
  var confirmNo = document.getElementById('confirmNo');
  modal.style = 'display: block';
  confirmYes.onclick = function () {
    document.getElementById('deleteValue').value = value;
    document.getElementById('deleteAction').submit();
  }
  confirmNo.onclick = function () { modal.style = 'display: none'; }
}

//  Dynamically calculates LTV value as loan amount and loan value fields are being typed in.
function calcltv() {
  var c_loan = parseFloat(document.getElementById('calcloan').value).toFixed(2);
  var c_value = parseFloat(document.getElementById('calcvalue').value).toFixed(2);
  var ltv = (c_loan / c_value) * 100;
  if (isNaN(ltv)) {
    document.getElementById('calcltv').innerHTML = "Please enter loan data";
  } else {
    if (c_loan > c_value) {
      document.getElementById('calcltv').innerHTML = "Loan value must be greater or equal to loan amount";
    } else {
      document.getElementById('calcltv').innerHTML = ltv.toFixed(2) + "%";
    }
  }
}

// Formats loan amount and loan value to float value with two decimal places and performs final calculation
function setfloat(idname) {
  document.getElementById(idname).value = parseFloat(document.getElementById(idname).value).toFixed(2);
  calcltv();
}

// Makes sure that the loan amount and loan value is valid before inserting data into database
function validateForm() {
  var c_loan = parseFloat(document.getElementById('calcloan').value);
  var c_value = parseFloat(document.getElementById('calcvalue').value);
  if (c_loan < 1 || c_value < 1 || c_loan > c_value) {
    document.getElementById('calcltv').innerHTML = "<span style='color:red;'>Invalid loan data</span>";
    return false;
  }
  return true;
}
