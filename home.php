<?php
/************************
 *
 *  File:  home.php
 *  Date:  May 17, 2020
 *  Author:  Scott DeLoach
 *  Project:  Loan-to-Value
 *  Description: HTML sub-template for home page.  Lists all clients in database with
 *  the option to add, edit and delete clients.  Deletes will pop up a modal
 *  dialog to verify if delete is desired.
 *
 ************************/
?>
<div id='deleteModal'>
  <h4>Are you sure you want to delete?</h4>
  <button type='button' id='confirmYes'>Yes</button>
  <button type='button' id='confirmNo'>No</button>
</div>
<h1>Clients</h1>
<form action='<?php $PHP_SELF; ?>' method='post'>
  <button type='submit' name='action' value='Add'>Add New Client</a></button>
</form><br>
<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>First Name</th>
      <th>Middle Initial</th>
      <th>Last Name</th>
      <th>Loan Amount</th>
      <th>Loan Value</th>
      <th>LTV</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $data->listclients(); ?>
  </tbody>
</table>
<form action='<?php $PHP_SELF; ?>' method='post'>
  <button type='submit' name='action' value='Add'>Add New Client</a></button>
</form>
<form id='deleteAction' action='<?php $PHP_SELF; ?>' method='post' style='display:none'>
  <input name='action' value='delete'>
  <input id='deleteValue' name='record' value=''>
</form>
