# LTV Table

LTV Table is a simple PHP application that stores LTV values into a MySQL database.

## Installation

Install the files into a LAMP environment and configure the database settings in library.php

Run application from your browser

## Contributing

Scott DeLoach - scott@blessedlogic.com
