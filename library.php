<?php
 /************************
  *
  *  File:  library.php
  *  Date:  May 17, 2020
  *  Author:  Scott DeLoach
  *  Project:  Loan-to-Value
  *  Description:  Private libary containing functions and classes used by
  *  this project.
  *
  ************************/


/************************
 *
 *  Type:  Function
 *  Name:  setValue
 *  Purpose:  Used to calculate the LTV value based on the loan and value data.
 *
 ************************/
 function setValue($name) {
   global $clientData;
   $retval = '';
   if ($name == 'ltv') {
     if (isset($clientData['loan']) && isset($clientData['value'])) {
       $retval = number_format((($clientData['loan'] / $clientData['value']) * 100), 2) . "%";
     } else {
       $retval = "Please enter loan data";
     }
   } else {
     $retval = (isset($clientData[$name]) ? $clientData[$name] : '');
   }
   return $retval;
 }

/************************
 *
 *  Type:  Class
 *  Name:  connectdb
 *  Purpose: Connects to MySQL database and provides basic CRUD methods.
 *
 ************************/
class connectdb {

  // Local properties
  private $conn; // Holds mysqli instance
	private $servername = "localhost";
	private $username = "root";
	private $password = "root";
	private $database = "ltv_table";

  function __construct() {
	   // Create database connection
   	$this->conn = new mysqli($this->servername, $this->username, $this->password, $this->database);
	  // Check connection
	  if ($this->conn->connect_error) {
	     die("Connection failed: " . $this->conn->connect_error . PHP_EOL);
	  }
  }

  function __destruct() {
    // Close connection when done
    mysqli_close($this->conn);
  }

  // Fetch client data and generate output of inner table rows
  function listclients() {
    $output = "";
    $sql = "SELECT * FROM test";
    $result = mysqli_query($this->conn,$sql);
    if (mysqli_num_rows($result) == 0) {
      $output = "<tr><td colspan='8' align='center'>No data found</td></tr>".PHP_EOL;
    } else {
      while($row = mysqli_fetch_array($result)) {
        list($id, $fname, $mname, $lname, $loan, $value) = $row;
        $ltv = number_format((($loan / $value) * 100), 2);
        $output .= "<tr><td>$id</td>".PHP_EOL;
        $output .= "<td>$fname</td>".PHP_EOL;
        $output .= "<td>$mname</td>".PHP_EOL;
        $output .= "<td>$lname</td>".PHP_EOL;
        $output .= "<td>$${loan}</td>".PHP_EOL;
        $output .= "<td>$${value}</td>".PHP_EOL;
        $output .= "<td>${ltv}%</td>".PHP_EOL;
        $output .= "<td><form action='' method='post'>".PHP_EOL;
        $output .= "<input type='hidden' name='action' value='edit'>".PHP_EOL;
        $output .= "<input type='hidden' name='record' value='$id'>".PHP_EOL;
        $output .= "<button type='submit' name='action' value='Edit'>Edit</a></button>".PHP_EOL;
        $output .= "<button type='button' onclick='verifyDelete($id);'>Delete</button>".PHP_EOL;
        $output .= "</form></td></tr>".PHP_EOL;
      }
    }
    echo $output;
  }

  // Prepares data for MySQL insert/update
  private function prepdata($input) {
    $data['fname'] = $this->conn->real_escape_string($input['fname']);
    $data['mname'] = $this->conn->real_escape_string($input['mname']);
    $data['lname'] = $this->conn->real_escape_string($input['lname']);
    $data['loan'] = (float)$input['loan'];
    $data['value'] = (float)$input['value'];
    return $data;
  }

  // Inserts client data
  function insert($input) {
    $data = $this->prepdata($input);
    $sql = "INSERT INTO test (first_name, middle_initial, last_name, loan, value)
    VALUES ('${data['fname']}', '${data['mname']}', '${data['lname']}', ${data['loan']}, ${data['value']})";
    if (mysqli_query($this->conn,$sql) === FALSE) {
      trigger_error("Unable to insert record.", E_USER_WARNING);
    };
  }

  // Updates client data
  function update($input) {
    $data = $this->prepdata($input);
    $id = $input['id'];
    $sql = "UPDATE test SET first_name = '${data['fname']}', middle_initial = '${data['mname']}', last_name = '${data['lname']}',
    loan = ${data['loan']}, value = ${data['value']} WHERE id = $id";
    if (mysqli_query($this->conn,$sql) === FALSE) {
      trigger_error("Unable to update record.", E_USER_WARNING);
    };
  }

  // Deletes client data
  function delete($id) {
    $sql = "DELETE FROM test WHERE id = $id";
    if (mysqli_query($this->conn,$sql) === FALSE) {
      trigger_error("Unable to delete record.", E_USER_WARNING);
    };
  }

  // Gets specific client data
  function get($id) {
    $sql = "SELECT * FROM test WHERE id = $id";
    $result = mysqli_query($this->conn,$sql);
    if (mysqli_num_rows($result) == 0) {
      $output = "<tr><td colspan='4' align='center'>No data found</td></tr>".PHP_EOL;
    }
    return mysqli_fetch_array($result);
  }
}
