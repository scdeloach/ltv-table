<?php
/************************
 *
 *  File:  index.php
 *  Date:  May 17, 2020
 *  Author:  Scott DeLoach
 *  Project:  Loan-to-Value
 *  Description:  A simple PHP project to demonstrate an interactive application
 *  utilizing classes, functions, and javascript in a single web page.  Utilizes
 *  a HTML template with will pull in a sub-template based on what action is
 *  taken.
 *
 ************************/

// Main Routine
require 'library.php';
$data = new connectdb();
$action = isset($_POST['action']) ? $_POST['action'] : 'home';
$submitValue = ($action == 'Edit' ? 'Update' : $action);
$actionValue = ($action == 'Edit' ? 'update' : 'insert');

// Load data based on current action
switch ($action) {
  case 'insert':
    $data->insert($_POST);
    $action = 'home';
    break;
  case 'update':
    $data->update($_POST);
    $action = 'home';
    break;
  case 'delete':
    $data->delete($_POST['record']);
    $action = 'home';
    break;
  case 'Edit':
    $clientData = $data->get($_POST['record']);
    break;
}

// Reset POST variables so that reloading page won't cause duplicates
if($action == 'home' && isset($_POST['action'])) header('location:'.$_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="style.css">
  <script src="scripts.js"></script>
</head>
<body>
<?php
  // Pull in sub-template based on action
  if ($action == 'home') {
    // Show the home page listing all clients
    include 'home.php';
  } else {
    // Show the add/update form for single client
    include 'client.php';
  }
?>
</body></html>
