<?php
/************************
 *
 *  File:  client.php
 *  Date:  May 17, 2020
 *  Author:  Scott DeLoach
 *  Project:  Loan-to-Value
 *  Description: HTML sub-template for client form.  Form is used for both adding
 *  and editing clients.  LTV field is dynamically calculated as the loan amount
 *  and loan value is entered.
 *
 ************************/
?>
<h1><?php echo $action; ?> Client</h1>
<form name='cform' method='post' action='<?php $PHP_SELF; ?>' onsubmit='return validateForm();'>
<input type='hidden' name='action' value='<?php echo $actionValue; ?>'>
<?php if ($action == 'Edit') { ?>
<input type='hidden' name='id' value='<?php echo $_POST['record']; ?>'>
<?php } ?>
<table class='clienttable'>
  <tr><th>Full Name</th>
    <td>
      <input type='text' name='fname' size='30' maxlength='30' value='<?php echo setValue("first_name"); ?>' required>
      <input type='text' name='mname' size='1' maxlength='1' value='<?php echo setValue("middle_initial"); ?>'>
      <input type='text' name='lname' size='30' maxlength='30' value='<?php echo setValue("last_name"); ?>'required>
    </td>
  </tr>
  <tr>
    <th>Loan Amount</th>
    <td>$<input id='calcloan' type='number' step='any' name='loan' size='11' maxlength='11' onkeyup='calcltv();' onchange='setfloat("calcloan");'  value='<?php echo setValue("loan"); ?>'required></td>
  </tr>
  <tr>
    <th>Loan Value</th>
    <td>$<input id='calcvalue' type='number' step='any' name='value' size='11' maxlength='11' onkeyup='calcltv();' onchange='setfloat("calcvalue");'  value='<?php echo setValue("value"); ?>'required></td>
  </tr>
  <tr><th>LTV</th><td id='calcltv'><?php echo setValue("ltv"); ?></td></tr>
  <tr class='grey'><td colspan='2' align='center'>
    <input type='submit' value='<?php echo $submitValue; ?>'>
    <a href='<?php $PHP_SELF; ?>'><button type='button'>Cancel</button></a>
  </td></tr>
</table>
</form>
